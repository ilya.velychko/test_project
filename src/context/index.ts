import { createContext } from "react";
import { IApplicationContext } from "../interfaces";

export const ApplicationContext = createContext<IApplicationContext>({
    handleHover() {},
    requestResult: [{
        name: "",
        field: 0
    }],
    setTableSize() {},
    tableSize: 0,
    setHoverElements() {},
    hoverElements: [],
    setButtonConfirm() {},
    isButtonConfirmed: false
});