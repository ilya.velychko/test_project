import React, { useState } from 'react';
import SelectMenu from './components/SelectMenu/SelectMenu';
import { useRequest } from "./hooks";
import { HoverHistoryList } from "./components/HoverHistoryList/HoverHistoryList";
import { HoverTable } from "./components/HoverTable/HoverTable";
import { handleHover } from "./helpers";
import { ApplicationContext } from "./context";
import './App.css';

function App() {
    const { requestResult } = useRequest();
    const [tableSize, setTableSize] = useState<number>(0);
    const [hoverElements, setHoverElements] = useState<any[]>([]);
    const [isButtonConfirmed, setButtonConfirm] = useState<boolean>(false);

    const fn = handleHover(setHoverElements);

    const handleButtonConfirm = () => {
        setButtonConfirm((prevState) => !prevState);
    }

    return (
        <ApplicationContext.Provider value={{
            handleHover: fn,
            requestResult,
            setTableSize,
            tableSize,
            setHoverElements,
            hoverElements,
            setButtonConfirm: handleButtonConfirm,
            isButtonConfirmed
        }}>
            <div className="App">
                <div className="left_side">
                    <SelectMenu />
                    <HoverTable />
                </div>
                <div className="right_side">
                    <h4>Hover Squares</h4>
                    <HoverHistoryList />
                </div>
            </div>
        </ApplicationContext.Provider>
  );
}

export default App;
