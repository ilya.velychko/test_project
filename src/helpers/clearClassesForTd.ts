export const clearClassesForTd = () => {
    for (let td of document.getElementsByTagName('td')) {
        if (td.hasAttribute('class')) {
            td.removeAttribute('class');
        }
    }
}