export const handleHover = (setHoverElements: (prevState: any) => void) => (event: any) => {
    if (event.target.getAttribute('class') === null) {
        event.target.setAttribute('class', 'hover');
        setHoverElements((prevState: any) => [{ row: event.target.dataset.row, col: event.target.dataset.col, id: event.target.dataset.tableElement }, ...prevState]);
    } else {
        event.target.removeAttribute('class');
        setHoverElements((prevState: any) => prevState.filter(({ id }: { id: any }) => id !== event.target.dataset.tableElement));
    }
}