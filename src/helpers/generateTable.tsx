export function generateTable(numberOfRowsAndCols: number) {
    const arr = [];
    for (let i = 1; i <= numberOfRowsAndCols; i++) {
        for (let j = 1; j <= numberOfRowsAndCols; j++) {
            arr.push({
                trId: i,
                trElements: {
                    row: i,
                    column: j,
                    id: j + '_' + i
                }
            });
        }
    }

    const formData = arr.reduce((previousValue: any, currentValue: any) => {
        return [...previousValue, {
            trId: currentValue.trId,
            trElements: [{
                ...currentValue.trElements
            }]
        }]
    }, []);

    return formData.reduce((previousValue, currentValue) => {
        const index = previousValue.findIndex((x: any) => x.trId === currentValue.trId);
        if (index === -1) {
            previousValue.push(currentValue);
        } else {
            previousValue[index]?.trElements.push(...currentValue.trElements);
        }
        return previousValue;
    }, []);
}