import { useContext } from "react";
import { ApplicationContext } from "../../context";

export const HoverHistoryList = () => {
    const { hoverElements } = useContext(ApplicationContext);
    return (
        <>
            {
                hoverElements ? (
                    <div className="hover_list">
                        {
                            hoverElements.map((element, index) => (
                                <div className="hover_list-element" key={index}>
                                    Row: {element.row} Col: {element.col}
                                </div>
                            ))
                        }
                    </div>
                ) : (
                    <div>No Data...</div>
                )
            }
        </>
    )
};