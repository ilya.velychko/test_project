import React, { useContext } from "react";
import { ApplicationContext } from "../../context";
import { clearClassesForTd } from "../../helpers";

export const SelectComponent = () => {
    const { requestResult, setTableSize, setHoverElements } = useContext(ApplicationContext);

    const selectMenuHandler = (e: React.ChangeEvent<HTMLSelectElement>) => {
        setTableSize(Number(e.target.value));
        setHoverElements([]);
        clearClassesForTd();
    }

    return requestResult ? (
        <select className="select_control" onChange={selectMenuHandler}>
            <option value="pick_mode">Pick Mode</option>
            {requestResult.map(({ field, name}: { field: number; name: string }) => (
                <option value={field} key={name}>{field}</option>
            ))}
        </select>
    ) : (
        <div>No Data...</div>
    )
}