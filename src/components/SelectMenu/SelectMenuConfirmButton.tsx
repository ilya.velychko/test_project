import React, {useContext} from "react";
import { ApplicationContext } from "../../context";

export const SelectMenuConfirmButton = () => {
    const { setButtonConfirm, isButtonConfirmed, tableSize } = useContext(ApplicationContext);

    return (
        <button className="select_menu_confirm_button" onClick={setButtonConfirm} disabled={tableSize === 0}>{isButtonConfirmed ? "STOP" : "START"}</button>
    )
}