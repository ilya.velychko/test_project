import React from "react";
import { SelectMenuConfirmButton } from "./SelectMenuConfirmButton";
import { SelectComponent } from "./SelectComponent";

const SelectMenu = () => {
    return (
        <div className="select_menu_wrapper">
            <SelectComponent />
            <SelectMenuConfirmButton />
        </div>
    )
}

export default SelectMenu;