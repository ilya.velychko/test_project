import React, { useContext } from "react";
import { generateTable } from '../../helpers';
import { ApplicationContext } from "../../context";

export const HoverTable = () => {
    const { handleHover, tableSize, isButtonConfirmed } = useContext(ApplicationContext);
    const generatedTable = generateTable(tableSize);

    const renderTable = () => (
        <>
            {
                generatedTable.map(({ trId, trElements }:{ trId: string; trElements: { row: string; column: string; id: string }[]}) => (
                    <tr key={trId}>
                        {
                            trElements.map(({ row, column, id }:{ row: string; column: string; id: string }) => (
                                <td data-row={row} data-col={column} data-table-element={id} key={id}></td>
                            ))
                        }
                    </tr>
                ))
            }
        </>
    );

    return (
        <table className="table">
            <tbody onMouseOver={isButtonConfirmed ? handleHover : () => {}}>
                {renderTable()}
            </tbody>
        </table>
    )
};