export interface IRequestResult {
    name: string;
    field: number;
}

export interface IHoverElement {
    row: string;
    col: string;
    id: string
}

export interface IApplicationContext {
    handleHover: (e: any) => void;
    requestResult: IRequestResult[];
    setTableSize: (value: number) => void;
    tableSize: number;
    setHoverElements: (elements: IHoverElement[]) => void;
    hoverElements: IHoverElement[];
    setButtonConfirm: (e: any) => void;
    isButtonConfirmed: boolean;
}

