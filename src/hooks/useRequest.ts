import { useEffect, useState } from "react";
import { IRequestResult } from '../interfaces'

export const useRequest = () => {
    const [requestResult, setRequestResult] = useState<IRequestResult[]>([]);

    useEffect(() => {
        fetch('http://demo7919674.mockable.io/')
            .then(response => response.json())
            .then(data => setRequestResult(data))
            .catch(err => console.log(err));
    }, []);

    return { requestResult };
}